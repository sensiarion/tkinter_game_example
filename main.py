import math
import tkinter
import random


def random_coords():
    return random.randint(0, N_X - 1) * step, random.randint(0, N_Y - 1) * step


def move_wrap(obj, move):
    canvas.move(obj, move[0], move[1])
    coords = canvas.coords(obj)

    x_out = coords[0] < 0 or coords[0] >= N_X * step
    y_out = coords[1] < 0 or coords[1] >= N_Y * step

    if x_out or y_out:
        canvas.move(obj, -move[0], -move[1])


def on_end(is_won):
    label.config(text='Ты проиграл!' if not is_won else "Победа!")
    prepare_and_start()


def always_right(obj):
    return (step, 0)


def random_move(obj):
    return random.choice([(step, 0), (-step, 0), (0, step), (0, -step)])


def move_to_player(obj):
    player_coords = canvas.coords(player)
    obj_coords = canvas.coords(obj)

    coords = [player_coords, obj_coords]
    coords.sort()

    move_x = math.copysign(step, player_coords[0] - obj_coords[0])
    move_y = math.copysign(step, player_coords[1] - obj_coords[1])

    return move_x, move_y


def ai():
    strategies = [move_to_player,always_right,random_move]
    for enemy in enemies:
        strategy = random.choice(strategies)
        move_wrap(enemy, strategy(enemy))


def check_move():
    if canvas.coords(player) == canvas.coords(exit):
        on_end(True)

    for f in enemies:
        if canvas.coords(player) == canvas.coords(f):
            on_end(False)


def key_pressed(event):
    if event.keysym == 'Up':
        move_wrap(player, (0, -step))
    if event.keysym == 'Down':
        move_wrap(player, (0, step))
    if event.keysym == 'Left':
        move_wrap(player, (-step, 0))
    if event.keysym == 'Right':
        move_wrap(player, (step, 0))

    ai()
    check_move()


def prepare_and_start():
    canvas.delete("all")
    global player_pos, exit_pos, player, exit
    player_pos = (random.randint(0, N_X - 1) * step,
                  random.randint(0, N_Y - 1) * step)
    exit_pos = (random.randint(0, N_X - 1) * step,
                random.randint(0, N_Y - 1) * step)

    N_ENEMIES = 6
    existing_coords = set()
    while len(existing_coords) < N_ENEMIES + 2:
        existing_coords.add(random_coords())

    player = canvas.create_image(existing_coords.pop(),
                                 image=player_pic, anchor='nw')
    exit = canvas.create_image(existing_coords.pop(),
                               image=exit_pic, anchor='nw')

    enemies.clear()
    for i in range(N_ENEMIES):
        fire_pos = existing_coords.pop()
        fire = canvas.create_image(
            (fire_pos[0], fire_pos[1]),
            image=enemy_pic, anchor='nw')
        enemies.append(fire)


player_pos, exit_pos, player, exit = None, None, None, None
enemies = []

master = tkinter.Tk()
master.title('Моя игра')

step = 60
N_X = 10
N_Y = 10
canvas = tkinter.Canvas(master, bg='gray',
                        width=step * N_X, height=step * N_Y)

player_pic = tkinter.PhotoImage(file='images/puting.gif')
exit_pic = tkinter.PhotoImage(file='images/navalnii.png')
enemy_pic = tkinter.PhotoImage(file='images/zombie.gif')

label = tkinter.Label(master, text="Найди выход")
label.pack()
canvas.pack()
master.bind("<KeyPress>", key_pressed)

prepare_and_start()

master.mainloop()
